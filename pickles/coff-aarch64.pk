/* coff-aarch64.pk - COFF implementation for GNU poke, aarch64.  */

/* Copyright (C) 2022, 2023, 2024, 2025 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Magic numbers.  */

var COFF_AARCH64_MAGIC = 0xaa64UH; /* From Microsoft specification.  */

coff_magic_numbers
  += [COFF_Magic_Numbers { arch = COFF_ARCH_AARCH64,
                           numbers = [COFF_AARCH64_MAGIC] }];

/* Aarch64 relocations.  */

type COFF_Aarch64_RELOC =
  struct
  {
    offset<uint<32>,B> r_vaddr;
    uint<32> r_symndx;
    uint<16> r_type;
    offset<uint<32>,B> r_offset;
  };
